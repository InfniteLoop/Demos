-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 27 Mai 2019 à 03:40
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

CREATE TABLE `cours` (
  `ID_cours` int(11) NOT NULL,
  `nom` varchar(150) CHARACTER SET latin7 NOT NULL,
  `description` varchar(500) NOT NULL,
  `categorie` varchar(15) NOT NULL,
  `URL` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `cours`
--

INSERT INTO `cours` (`ID_cours`, `nom`, `description`, `categorie`, `URL`) VALUES
(11111, 'Initiation ą Ruby', 'Les principes de base pour commencer à programmer en Ruby', 'programmation', 'ruby.html'),
(123456, 'Python pour débutant', 'La base pour apprendre &agrave; coder en Python', 'programmation', 'python.html'),
(741741, 'Les bases en c++', 'Comment coder ses premières lignes en c++.', 'programmation', 'cplusplus.html'),
(753753, 'Bash pour les nulls', 'Les commandes de base pour naviguer dans le terminal', 'linux', 'bash.html'),
(987654, 'Les injections en SQL', 'Pour bien comprendre les injections en SQL', 'securite', 'SQLinjection.html');

-- --------------------------------------------------------

--
-- Structure de la table `questions`
--

CREATE TABLE `questions` (
  `ID` int(10) NOT NULL,
  `question` varchar(400) NOT NULL,
  `reponse` varchar(400) NOT NULL,
  `ID_cours` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `questions`
--

INSERT INTO `questions` (`ID`, `question`, `reponse`, `ID_cours`) VALUES
(1, 'Quelle est la m&eacute;thode pour afficher le string "Bonjour" en Python?', 'print("Bonjour")', 123456),
(2, 'Quelle est la m&eacute;thode qui permet de r&eacute;cup&eacute;rer la r&eacute;ponse de l\'utilisateur?', 'input', 123456),
(3, 'Vrai ou faux? Nous ne pouvons pas imbriquer plusieurs if dans la m&ecir;me fonction.', 'Faux', 741741),
(4, 'Quelle m&eacute;thode nous permet d\'afficher plusieurs chaines de caract&egrave;re sur la même ligne (sans ajout de saut de ligne)?', 'print', 11111),
(5, 'Vrai ou faux? On peut ajouter des attributs aux commandes.', 'Vrai', 753753),
(6, 'Quelle commande est l\'&eacute;quivalent d\'un manuel d\'instruction?', 'man', 753753),
(7, 'Quelle commande nous permet d\'effacer un fichier?', 'rm', 753753),
(9, 'Nommer la première condition de décision en C++et autre langage?', 'if', 741741),
(11, 'Oui ou non ? A -t- on besoin d\'une condition pour le else ?', 'non', 741741),
(13, 'Vrai ou faux ? Les injections SQL sont la vulnérabilité la plus populaire?', 'vrai', 987654),
(15, 'Vrai ou faux? le 1=1 retourne toujours vrais dans une requête pour faire une injection SQL?', 'vrai', 987654),
(17, 'Vrai ou faux? les déclarations préparées sont une protection contre les injections SQL?', 'vrai', 987654);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `mdp` varchar(255) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nb_cours_faits` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`username`, `mdp`, `prenom`, `nom`, `email`, `nb_cours_faits`) VALUES
('test', 'test', 'test', 'test', 'test@test.com', 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `cours`
--
ALTER TABLE `cours`
  ADD PRIMARY KEY (`ID_cours`);

--
-- Index pour la table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `questions`
--
ALTER TABLE `questions`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
