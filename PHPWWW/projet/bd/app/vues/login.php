<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href="./css/style.css">

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>LogiCode</title>
</head>
<body>
<div>
<?php
	include_once("banner.php");
?>
	<div>
	<?php
	if (isset($_REQUEST["global_message"]))
		$msg="<span class=\"warningMessage\">".$_REQUEST["global_message"]."</span>";
	
	$u = "";
	if (isset($_REQUEST["username"])) {
		$u = $_REQUEST["username"];
	}
	?>
	<div id="LoginForm">
    <h2>Connexion</h2>
    <form action="" method="post" class = "Login">
    	<label for="username"> Nom utilisateur: </label><br />
    	<input type="text" name="username" value="<?php echo $u?>" />
    	<?php 
    	if( isset($_REQUEST["field_messages"]["username"])) {
    		echo "<br /><span class=\"warningMessage\">".$_REQUEST["field_messages"]["username"]."</span>";
    	}
    	?>
    	<br />
    	<label for="password">Mot de passe: </label><br />
    	<input type="password" name="password">
    	<?php 
    	if (isset($_REQUEST["field_messages"]["password"])) {
    		echo "<br /><span class=\"warningMessage\">".$_REQUEST["field_messages"]["password"]."</span>";
    	}
    	?>
    	<br />
        <br />
    	<input type="hidden" name="action" value="connecter">
    	<input type="submit" value="Connecter">
    </form>		
	
	</div>
	</div>
</div>    
</body>
</html>
