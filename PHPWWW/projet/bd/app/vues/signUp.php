<!DOCTYPE html>
<html>
<head>
	<title>LogiCode</title>
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<script src="./js/js.js" ></script>
</head>
<body>
<div>
	<?php
	include("./vues/banner.php");
	?>
	<?php
	$u = "";
	if (isset($_REQUEST["username"]))
		$u = $_REQUEST["username"];
?>
	<div id="signupForm">
		<h2>Inscription</h2>
		<form action="" method="post"class = "signUp">
			<label for="username">Nom utilisateur</label><br />
			<input type="text" name="username" value="<?php echo $u?>"/><br />
			<?php 
			if (isset($_REQUEST["field_messages"]["username"])) {
				echo "<span class=\"warningMessage\">".$_REQUEST["field_messages"]["username"]."</span>";
			}
    		?>
			<br />

			<label for="firstname">Prénom: </label><br />
			<input type="text" name="prenom"/><br />
			<?php 
			if (isset($_REQUEST["field_messages"]["prenom"])) {
				echo "<span class=\"warningMessage\">".$_REQUEST["field_messages"]["prenom"]."</span>";
			}
    		?>
		    <br />
			<label for="lastname">Nom: </label><br />
			<input type="text" name="nom"/><br />
			<?php 
			if (isset($_REQUEST["field_messages"]["nom"])) {
				echo "<span class=\"warningMessage\">".$_REQUEST["field_messages"]["nom"]."</span>";
			}
    		?>
			<br />
			<label for="email">Email: </label><br />
			<input type="text" name="email"/><br />
			<?php 
			if (isset($_REQUEST["field_messages"]["email"])) {
				echo "<span class=\"warningMessage\">".$_REQUEST["field_messages"]["email"]."</span>";
			}
    		?>
			<br />

			<label for="password">Mot de passe: </label><br />
			<input type="password" name="password"/><br />

			<?php 
			if (isset($_REQUEST["field_messages"]["password"])) {
				echo "<span class=\"warningMessage\">".$_REQUEST["field_messages"]["password"]."</span>";
			}
    		?>
			<br />

			<label for="pass-repeat">Comfirmation du mot de passe: </label><br />
			<input type="password" name="pass-repeat"/><br />
			<?php 
			if (isset($_REQUEST["field_messages"]["pass-repeat"])) {
				echo "<span class=\"warningMessage\">".$_REQUEST["field_messages"]["pass-repeat"]."</span>";
			}
			?>
			<br />

			<input type="hidden" name="action" value="inscrire"/>
			<input type="submit"  value="Inscription" />

		</form>
	</div>
</div>
</body>
</html>