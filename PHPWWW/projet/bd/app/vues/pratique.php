<?php
include("./vues/banner.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pratique</title>
</head>
<body>
    <!-- Create a simple CodeMirror instance -->
<link rel="stylesheet" href="lib/codemirror-5.46.0/lib/codemirror.css">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<script src="lib/codemirror-5.46.0/lib/codemirror.js"></script>

    <form action="" method="post">
        <tr>
            <td>
                language: 
	    <select id ="langId" name="language">
            <option value="1">Python 3</option>
            <option value="2">Ruby</option>
            <option value="8">C++</option>
            <option value="11">Bash</option>
            
        </select>
        </td>
        </tr>
        <br>
        <br>
        <!--<input id="code"type="textarea" name="code" value="<//?php echo $_REQUEST['code']?>" >-->
        <textarea id='code' name = 'code' rows='10' cols='80'><?php echo $_REQUEST['code']?></textarea>
        <br>
        <input type="submit" value="ok" name="submit">
    </form>
    <script>
        var editor = CodeMirror.fromTextArea(document.getElementById('code'),{
            matchBrackets: true,
            lineNumbers: true,
            readOnly: false,
            indentUnit: 4,
            scrollbarStyle: null
            });
        editor.setSize('100%', Math.max(100, editor.getScrollInfo().height)); 
</script>
</body>
</html>
<?php
if (isset($_REQUEST["submit"])) {
    
    //$url_rc="http://localhost:12380/compile";
    $url_rc="http://172.29.0.1:12380/compile";
    $data_rc=array('language' => $_REQUEST["language"], 'code' => $_REQUEST["code"], 'parameters' => "", 'stdin' =>"", 'vm_name'=>"remotecompiler");
    $options_rc=array('http'=> array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data_rc)));
    $context=stream_context_create($options_rc);
    $comp_resp=file_get_contents($url_rc, false, $context);
    //echo $comp_resp;
    $array = json_decode($comp_resp,true);
    //print_r($array);
    if($array["errors"] == ""){
        echo "Sortie: ".$array["output"];
    }
    else {
        echo "Erreurs: ".$array["errors"];
    }
    }

?>