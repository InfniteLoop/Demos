
<html>

<head>
<meta http-equiv="Content-Language" content="en-ca">
<meta http-equiv="Content-Type" content="text/html; charset=charset=UTF-8">
<link rel="stylesheet" href="../style/style_accueil.css" type="text/css" />
<title>Votre profil</title>
<style>
#myProgress {
  width: 100%;
  background-color: #ddd;
 
}

#maBarre {
  width: 1%;
  height: 30px;
  background-color: blue; 
  
}


.domaine{
	text-indent : 0px;
	padding : 50px;
}
.domaine:hover{
	color: blue;
	background-color:white;
	
}
</style>

</head>

<body>

<?php
require_once('./modele/classes/User.class.php');
require_once('./modele/UserDAO.class.php');
require_once('./modele/classes/Cours.class.php');
require_once('./modele/coursDAO.class.php');

include("banner.php");

	echo"<h3>Votre profil</h3>";
$dao = new UserDAO();

$nom_user=$_SESSION['connected'];
$utilisateur = $dao->find($nom_user);
//$utilisateur = $dao->find("test");
if($utilisateur == null){
echo"<p> pas trouv&aecute; </p>";
	
}
?>
<div>

	<div id="content">
		<table id='alisteCours'>


	<div class="container">
	<div class="row">
	<div class="panel cours domaine">
		<p>Votre nom d'utilisateur :<br /> <?=$utilisateur->getUsername()?></p><br />
		<p>Votre nom :<br /> <?=$utilisateur->getPrenom()?> 
		<?=$utilisateur->getNom()?></p><br />
		<p>Votre courriel :<br /> <?=$utilisateur->getEmail()?></p><br />
		
		</div>
		</div>

	</div>
	
<?php
	$c_dao = new CoursDAO();
	$c_total = $c_dao->findAll();

	$c_nb= sizeof($c_total);
	$c_fait=$utilisateur->get_nb_cours_faits();
	$v= ($c_fait/$c_nb)*100;
	
?>
	<div class =row>
	<div class="domaine">
	<p> Votre progression </p>
	<p><?=round($v,2) ?>%</p>
	
		<div id="myProgress">
			<div id="maBarre"></div>
		</div>
	
	</div>
	
	</div>
	</div>

</div>



<script>
var vr =<?php echo json_encode($v);?>;

function ajusteBarre() {
  var elem = document.getElementById("maBarre");   
  var width = 0;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= vr ) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
    }
  }
}
window.onload = ajusteBarre;

</script>

</body>
</html>
