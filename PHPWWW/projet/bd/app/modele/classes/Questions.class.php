<?php
class Questions
{
    private $ID;
    private $question;
    private $reponse;
    
    public function __construct(){

    }

    public function getID(){
        return $this->ID;
    }
	
	public function getID_cours(){
        return $this->ID_cours;
    }

    public function getQuestion(){
        return $this->question;
    }

    public function getReponse(){
        return $this->reponse;
    }

    public function setID($ID){
        $this->ID = $ID;
    }
	
	public function setID_cours($ID){
        $this->ID_cours = $ID;
    }

    public function setQuestion($ques){
        $this->$question = $ques;

    }

    public function setReponse($rep){
        $this->$reponse = $rep;
    }

    public function loadFromRecord($ligne)
	{
		$this->ID = $ligne["ID"];
		$this->question = $ligne["question"];
		$this->reponse = $ligne["reponse"];
		$this->ID_cours = $ligne["ID_cours"];
		
    }
    
    public function loadFromObject($ques_load)
	{
		$this->ID = $ques_load->ID;
		$this->question = $ques_load->question;
		$this->reponse = $ques_load->reponse;
		$this->ID_cours = $ques_load->ID_cours;
		
		
	}

    
    
}

?>