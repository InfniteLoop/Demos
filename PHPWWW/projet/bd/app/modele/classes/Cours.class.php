<?php

/**
*
*Description de la classe cours
*/

class Cours{
	
	private $nom;
	private $description;
	private $code;
	private $categorie;
	private $url_texte;
	/*
	private $qu1;
	private $qu2;
	private $qu3;
	private $qu4;
	private $qu5;
	private $rep1;
	private $rep2;
	private $rep3;
	private $rep4;
	private $rep5;
	*/
	
	public function __construct()	//Constructeur
	{		
		
	}
	
	public function getNom()
	{
			return $this->nom;
	}
	
	public function getDescription()
	{
			return $this->description;
	}
	
	public function getCode()
	{
			return $this->code;
	}
	
	public function setNom($nom_cours)
	{
		$this->nom = $nom_cours;
	}
	
	public function setDescription($description_cours)
	{
		$this->description = $description_cours;
	}
	public function setCode($code_cours)
	{
		$this->code = $code_cours;
	}
	
	public function getCategorie(){
		return $this->categorie;
	}
	
	public function setCategorie($value){
		$this->categorie = $value;
	}
	
	public function getUrlText(){
		return $this->url_texte;
	}
	
	public function setUrlText($value){
		$this->url_texte= $value;
	}
	public function __toString()
	{
		return "Code : ".$this->code." Nom :".$this->nom." Description :".$this->description;
	}
	
	public function afficher(){
		echo $this->__toString();
	}
	
	public function loadFromRecord($ligne)
	{
		$this->code = $ligne["ID_COURS"];
		$this->nom = $ligne["NOM"];
		$this->description = $ligne["DESCRIPTION"];
		$this->categorie = $ligne["CATEGORIE"];
		$this->url_texte=$ligne["URL_TEXTE"];
	}	
	public function loadFromObject($cours_load)
	{
		$this->code = $cours_load->ID_cours;
		$this->nom = $cours_load->nom;
		$this->description = $cours_load->description;
		$this->categorie = $cours_load->categorie;
		$this->url_texte= $cours_load->URL;
		
	}
}


?>