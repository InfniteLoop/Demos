<?php

class User {
	private $username = "";
	private $password = "";
	private $prenom = "";
	private $nom = "";
	private $email = "";
	private $nb_cours_faits =0;

	public function __construct()	//Constructeur
	{/*
		$this->username = $id;
		$this->prenom = $prenom_id;
		$this->nom = $nom_id;
		$this->email = $email_id;
		*/
	}	
	
	public function get_nb_cours_faits(){
		return $this->nb_cours_faits;
	}
	
	public function cours_reussi(){
		$this->nb_cours_faits++;
	}
	
	public function set_nb_cours_faits($value){
		$this->nb_cours_faits=$value;
	}
	
	public function getUsername()
	{
			return $this->username;
	}
	
	public function setUsername($value)
	{
			$this->username = $value;
	}
        
	public function getPassword()
	{
			return $this->password;
	}
	
	public function setPassword($value)
	{
			$this->password = $value;
	}
	
	public function getEmail()
	{
		return $this->email;		
	}
	public function setEmail($value)
	{
			$this->email = $value;
	}
	
	public function getNom()
	{
		return $this->nom;		
	}
	public function setNom($value)
	{
		$this->nom = $value;
	}
	public function setPrenom($value)
	{
		$this->prenom = $value;
	}
	
	public function getPrenom()
	{
		return $this->prenom;
	}

	public function __toString()
	{
		return "Utilisateur[".$this->prenom.",".$this->nom.",".$this->username.",".$this->email."]";
	}
	public function affiche()
	{
		echo $this->__toString();
	}
	public function loadFromRecord($ligne)
	{
		$this->username = $ligne["USERNAME"];
		$this->password = $ligne["MDP"];
		$this->nom = $ligne["NOM"];
		$this->prenom = $ligne["PRENOM"];
		$this->email = $ligne["EMAIL"];
		$this->nb_cours_faits = $ligne["NB_COURS_FAITS"];
	}	
	public function loadFromObject($user_load){
		$this->username = $user_load->username;
		$this->nom = $user_load->nom;
		$this->prenom = $user_load->prenom;
		$this->email = $user_load->email;
		$this->password = $user_load->password;
		$this->nb_cours_faits = $user_load->nb_cours_faits;
		
	}
}
?>