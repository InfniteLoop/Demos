<?php
include_once('./modele/classes/Database.class.php'); 
include_once('./modele/classes/User.class.php'); 

class UserDAO
{	
	public static function find($username)
	{
		$db = Database::getInstance();

		$pstmt = $db->prepare("SELECT * FROM users WHERE username = :x");
		$pstmt->execute(array(':x' => $username));
		
		$result = $pstmt->fetch(PDO::FETCH_OBJ);
		$p = new User();

		if ($result)
		{
			$p->setUsername($result->username);
			$p->setPassword($result->mdp);
			$p->setPrenom($result->prenom);
			$p->setNom($result->nom);
			$p->setEmail($result->email);
			$pstmt->closeCursor();
			return $p;
		}
		$pstmt->closeCursor();
		return null;
	}

		public function create($username, $password, $prenom, $nom, $email) {
				$request = "INSERT INTO users (USERNAME ,MDP,PRENOM,NOM,EMAIL)".
						" VALUES ('".$username."','".$password."','".$prenom."','".$nom."','".$email."')";
				try
				{
					$db = Database::getInstance();
					return $db->exec($request);
				}
				catch(PDOException $e)
				{
					throw $e;
				}
			}	
}
?>