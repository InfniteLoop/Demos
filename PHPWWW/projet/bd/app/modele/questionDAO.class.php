<?php
include_once("./modele/classes/Database.class.php");
include_once('./modele/classes/Questions.class.php');

class questionDAO{

    public function create($ques){
        
        $request = "INSERT INTO questions (ID,QUESTION,REPONSE,ID_cours)"." VALUES('".$ID->getID()."','".$ques->getQuestion()."','".$ques->getReponse()."','".$ques->getID_cours().")";
        
        try
        {
            $db = Database::getInstance();
			return $db->exec($request);
        }
    
        catch(PDOException $e)
		{
			throw $e;
		}
    
    
    }
	
	
	public static function find($id_question)
	{
		
		
		$db = Database::getInstance();

		$pstmt = $db->prepare("SELECT * FROM questions WHERE id = :x");
		$pstmt->execute(array(':x' => $id_question));
		
		$result = $pstmt->fetch(PDO::FETCH_OBJ);
		$p = new Questions();

		if ($result)
		{
			$p->setID($result->ID);
			$p->setQuestion($result->question);
			$p->setReponse($result->reponse);
			
			$pstmt->closeCursor();
			return $p;
		}
		$pstmt->closeCursor();
		return null;
	}



public static function find_by_cours($num_cours)
	{
		
		$db = Database::getInstance();
            $lquestions = Array();
            try {
                $pstmt = $db->prepare("SELECT * FROM questions WHERE ID_cours= :x");
				
                $pstmt->execute(array(':x' => $num_cours));

                while ($result = $pstmt->fetch(PDO::FETCH_OBJ))
                {
                        $f = new Questions();
                        $f->loadFromObject($result);
                        array_push($lquestions, $f);
                }
                $pstmt->closeCursor();
                $pstmt = NULL;
                Database::close();
            }
            catch (PDOException $ex){
            }             
            return $lquestions;
		
	}	
	}	

?>