<?php
include_once('./modele/classes/Database.class.php'); 
include_once('./modele/classes/Cours.class.php'); 

class coursDAO
{	
	public function create($x) {
/*		
*/		$request = "INSERT INTO cours (ID_COURS ,NOM ,DESCRIPTION, CATEGORIE, URL)".
				" VALUES ('".$x->getCode()."','".$x->getNom()."','".$x->getDescription()."'".$x->getCategorie."'".$x->getUrlText.")";
		try
		{
			$db = Database::getInstance();
			return $db->exec($request);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
	}

	public static function findAll()
	{
		
		
		$db = Database::getInstance();
            $lcours = Array();
            try {
                $pstmt = $db->prepare("SELECT * FROM cours");
                $pstmt->execute();

                while ($result = $pstmt->fetch(PDO::FETCH_OBJ))
                {
                        $f = new Cours();
                        $f->loadFromObject($result);
                        array_push($lcours, $f);
                }
                $pstmt->closeCursor();
                $pstmt = NULL;
                Database::close();
            }
            catch (PDOException $ex){
            }             
            return $lcours;
		
	}	
	public static function find_by_categories($categorie)
	{
		
		$db = Database::getInstance();
            $lcours = Array();
            try {
                $pstmt = $db->prepare("SELECT * FROM cours WHERE categorie= :x");
				
                $pstmt->execute(array(':x' => $categorie));

                while ($result = $pstmt->fetch(PDO::FETCH_OBJ))
                {
                        $f = new Cours();
                        $f->loadFromObject($result);
                        array_push($lcours, $f);
                }
                $pstmt->closeCursor();
                $pstmt = NULL;
                Database::close();
            }
            catch (PDOException $ex){
            }             
            return $lcours;
		
	}	

	public static function find_par_code($id)
	{
		
		
		$db = Database::getInstance();

		$pstmt = $db->prepare("SELECT * FROM cours WHERE id_cours = :x");
		$pstmt->execute(array(':x' => $id));
		
		$result = $pstmt->fetch(PDO::FETCH_OBJ);
		$p = new Cours();

		if ($result)
		{
			$p->setCode($result->ID_cours);
			$p->setNom($result->nom);
			$p->setDescription($result->description);
			$p->setCategorie($result->categorie);
			$p->setUrlText($result->URL);
			//$p->set_nb_cours_faits($result->nb_cours_faits);
			$pstmt->closeCursor();
			return $p;
		}
		$pstmt->closeCursor();
		return null;
	}
	
	
	
	
}
?>