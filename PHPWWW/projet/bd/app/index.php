<?php
	
	if (!ISSET($_SESSION)) session_start();
// -- Contrôleur frontal --
require_once('./controleur/ActionBuilder.class.php');
if (ISSET($_REQUEST["action"]))
	{
		
		$actionDemandee = $_REQUEST["action"];
		$controleur = ActionBuilder::getAction($actionDemandee);
		$vue = $controleur->execute();
		/**/
	}
else	
	{
		$action = ActionBuilder::getAction("");
		$vue = $action->execute();
	}
// On affiche la page (vue)
include_once('./vues/'.$vue.'.php');
?>
