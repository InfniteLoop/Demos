<?php
require_once('./controleur/DefaultAction.class.php');
require_once('./controleur/AfficherCoursAction.class.php');
require_once('./controleur/AfficherCategorieAction.class.php');
require_once('./controleur/AfficherLeCoursAction.class.php');
require_once('./controleur/QuestionAction.class.php');
require_once('./controleur/AfficherProfilAction.class.php');
require_once('./controleur/LoginAction.class.php');
require_once('./controleur/LogoutAction.class.php');
require_once('./controleur/InscriptionAction.class.php');
require_once('./controleur/PratiqueAction.class.php');



class ActionBuilder{
	public static function getAction($action_demandee){
		switch ($action_demandee)
		{
			case "connecter" :
				return new LoginAction();
			break; 
			case "deconnecter" :
				return new LogoutAction();
			break; 
			case "afficher_cours" :
			//afficher les cours d'une categories
				return new AfficherCoursAction();
			break; 
			case "afficher_le_cours" :
			//afficher un cours precis
				return new AfficherLeCoursAction();
			break; 
			case "afficher_profil" :
			//afficher le profil de l'utilisateur
				return new AfficherProfilAction();
			break;
			case "afficher_categories" :
			//afficher les categories de cours possibles
				return new AfficherCategoriesAction();
			break;
			case "afficher_question" :
			 
			return new QuestionsAction();
			break; 			
			case "inscrire" :
				return new InscriptionAction();
			break;
			case "pratique" :
				return new PratiqueAction();
			break;
			case "affichage" :
				return new AffichageAction();
			break;
			default :
				return new DefaultAction();
		}
	}
}
?>