<?php
require_once('./controleur/Action.interface.php');
require_once('./modele/classes/User.class.php');
require_once('./modele/UserDAO.class.php');
class InscriptionAction  implements Action{
    public function execute(){
        if (!ISSET($_SESSION)) session_start();
        if (ISSET($_SESSION["connected"])) {
            return "default";
        }
        
        if (!ISSET($_REQUEST["username"])) {
            return "signUp"; 
        }
        if (!$this->valide()) {
            return "signUp";
        }
        $dao = new UserDAO();
        $user = new User();
        $user->setUsername($_REQUEST["username"]);
        $user->setPassword($_REQUEST["password"]);
        $user->setPrenom($_REQUEST["prenom"]);
        $user->setNom($_REQUEST["nom"]);
        $user->setEmail($_REQUEST["email"]);
        
        if (!$dao->create($user->getUsername(),$user->getPassword(),$user->getPrenom(),$user->getNom(),$user->getEmail())) {
            $_REQUEST["field_messages"]["username"] = "Cet utilisateur existe déjà!";
            return "signUp";
        }
        return "login";
    }

    public function valide(){

        $result = true;
        if ($_REQUEST["username"]=="") {
            $_REQUEST["field_messages"]["username"]="Vous devez avoir un nom d'utilisateur.";
            $result =false;
        }
        if ($_REQUEST["prenom"]=="") {
            $_REQUEST["field_messages"]["prenom"]="Prénom obligatoire.";
            $result =false;
        }
        if ($_REQUEST["nom"]=="") {
            $_REQUEST["field_messages"]["nom"]="Nom obligatoire.";
            $result =false;
        }
        if ($_REQUEST["email"]=="") {
            $_REQUEST["field_messages"]["email"]="Email obligatoire.";
            $result =false;
        }
        if ($_REQUEST["password"]=="") {
            $_REQUEST["field_messages"]["password"]="Mot de passe Obligatoire";
            $result =false;
        }
        if ($_REQUEST["password"] != $_REQUEST["pass-repeat"]) {
            $_REQUEST["field_messages"]["pass-repeat"]="Les mots de passe ne sont pas identique.";
            $result =false;
        }
        return $result;
    }
}

?>