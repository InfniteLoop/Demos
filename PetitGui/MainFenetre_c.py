from PySide2 import QtWidgets

import MainFenetre_f, objAssur, objToml, sys

oAssur = objAssur.AssurObj() 
oConfig = None
ver="1.0"

class MyQtApp(MainFenetre_f.Ui_MainWindow, QtWidgets.QMainWindow):

    def __init__(self):
        super(MyQtApp, self).__init__()
        self.setupUi(self)
        self.btnTraitement.setEnabled(1)
        self.checkBox.clicked.connect(oAssur.modifCheckBox)                    
        self.progressBar.setValue(0)
        self.btnTraitement.clicked.connect(oAssur.Demarrer)    
        self.btnPathAssur.clicked.connect(oAssur.PathAssur)
        self.btnPathOMHM.clicked.connect(oAssur.PathOMHM)
        self.setWindowTitle("Assur Plus © - Version " + ver)
    
def PrepareFenetre(qt_app):
    oAssur.setConsole(qt_app)    #Afin que l'objet oAssur puisse utiliser les objets graphiques.
    oAssur.verifierTOML()           
    oConfig = objToml.config()           #Créer l'objet Toml qui gère entièrement le fichier de config.
    
    if oConfig == None:
        return 0
    else:
        
        oAssur.setConfig(oConfig)          #Passer l'objet Toml a l'objet Assur afin qu'il puisse l'utiliser.
        
        qt_app.checkBox.setChecked(oAssur.CheckBox())
        
        #Désactiver le bouton [Demarrer]
        qt_app.txtPathAssur.setText(oAssur.verifierFicAssur("toml"))  
        qt_app.txtPathOMHM.setText(oAssur.verifierFicOMHM("toml")) 
        qt_app.btnTraitement.setEnabled(qt_app.txtPathOMHM.text() != '' and qt_app.txtPathAssur.text() != '') 
        return 1

if __name__ == '__main__':
    app = QtWidgets.QApplication()
    qt_app = MyQtApp()
    try:
        if PrepareFenetre(qt_app) == 1:
            qt_app.show()
            app.exec_()
    except:
        pass           