import toml

class config():
    config = None

    def __init__(self):
        self.config = toml.load("./config.toml")
        
        #LOAD le fichier TOML
    
    def GET(self, Section, Cle):
        #Vérifier si la section/clé existe, sinon MsgAvert()
        return self.config[Section][Cle]
        
    def REPLACE(self, Section, Cle, Valeur):
        self.config[Section][Cle] = Valeur

        with open("./config.toml", mode='w', encoding="utf-8") as w:
            w.write(toml.dumps(self.config)) 
        
        #Remplace la valeur dans la bonne section/clé
        #Sauvegarde le fichier sur HD en validant si le fichier est ouvert (sinon MsgAvertissement)